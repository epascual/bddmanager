#ifndef BDD_H
#define BDD_H


#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include "Node.h"
#include "Utils.h"
#include "Constants.h"

using namespace std;

class BDD
{
private:
  string atoms;
  Node * root;

  BDD(){}

  /*make_tree: Funci�n auxiliar del constructor.
    Precondiciones de make_tree
    assert(N <= atoms.size());
    assert(N <= log2(truthTable.size()));
    depth=0;
    numLeaf=0; This is an private attribute
  */
  Node * make_tree(int depth, int N, string atoms, const vector<bool> &truthTable,
                   Node *parent, Node *True, Node *False);

  ///deep_copy: Funci�n auxiliar del constructor de copia*/
  static Node * deep_copy(Node *nod, Node *parent, map<Node *, Node *> &oldToNewNode);
  ///free_memory: funci�n auxiliar para el destructor
  static void free_memory(Node *);
  ///free_orphans: si un nodo se vuelve inalcanzable, libera su memoria
  static void free_orphans(Node *);

  ///Subprocedimientos para la reducci�n
  bool step1(Node *);
  bool step2(Node *);
  Node * step2_aux(Node *, const Node *);

  ///Variable usada solo en make_tree
  int numLeaf;

  ///Algunas funciones auxiliares
  void size_aux(Node *nod, set<Node *> &allNodes);
  bool evaluate_interpretation_aux(Node* nod, const vector<bool> &interpretation);
  bool evaluate_interpretation_aux(Node* nod, bool interpretation[]);

  static bool is_before_in_ordering(char lit1, char lit2, const string &atoms);
  static Node * apply_aux(Node *bdd1, Node *bdd2, const string &atoms, vector<bool> op, Node *parent, Node *True, Node *False);

  ///Encuentra nodos �tomos, funci�n auxiliar utilizada en la negaci�n
  static void find_atoms_nodes(Node *, Node * &, Node * &);

  void restrict_aux(Node *nod, char atom, bool w);

  ///Funci�n para imprimir el �rbol
  void traverse(Node *nod, bool flag=0);

  ///Operador de asignacion privado para evitar asignaciones (como paso por valor en funciones)
  ///Solucion alternativa a tenerlo privado es implementarlo un deep_copy
  BDD& operator= (const BDD &otro);

public:

  /***********************************
   * PARTE PUBLICA O INTERFAZ DE BDD *
   ***********************************/
  BDD(int N, string atoms, vector<bool> &truthTable);
  BDD(const BDD &otro);
  virtual ~BDD();

  bool evaluate_interpretation(const vector<bool> &interpretation);
  bool evaluate_interpretation(bool interpretation[]);

  string getAtoms();

  int size();

  void reduce();

  BDD apply(const BDD &bdd2, vector<bool> op);
  BDD apply_negation();

  BDD restrict(char atom, bool w);

  void print();

  /***********************************
   * FIN DE INTERFAZ                 *
   ***********************************/
};

#endif // BDD_H
