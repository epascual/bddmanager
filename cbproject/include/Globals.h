#ifndef GLOBALS_H_INCLUDED
#define GLOBALS_H_INCLUDED

#include <vector>

using namespace std;

extern vector<bool> op_or;
extern vector<bool> op_and;
extern vector<bool> op_implication;
extern vector<bool> op_equivalence;
extern vector<bool> op_nor;
extern vector<bool> op_nand;
extern vector<bool> op_xor;

#endif // GLOBALS_H_INCLUDED
