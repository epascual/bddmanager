#ifndef NODE_H
#define NODE_H

#include <vector>
using namespace std;

class Node
{
private:
  char atom;

  Node *leftSon;
  Node *rightSon;

  ///Listas de aristas que llegan a este nodo
  vector<Node *> parents;

public:
  Node(char atom, Node *parent);
  virtual ~Node();

  bool operator == (const Node & o) const;

  char getAtom() const;
  void setAtom(char);

  void setLeftSon(Node *);
  void setRightSon(Node *);
  Node * getLeftSon() const;
  Node * getRightSon() const;

  vector<Node *> getParents() const;
  void addParent(Node *);
  void changeParent(Node *oldParent, const vector<Node*> &newParents);

  void deleteFromParents();
  void replaceInParents(Node *newSon);
};

#endif // NODE_H
