#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <assert.h>
#include <cstring>
#include <string>

#include <algorithm>
#include <iterator>

using namespace std;

/**
  Precondición: order1 y order2 son ordenes compatibles.
*/
string make_compatible_order(const string &order1, const string &order2);

/**
  Para tokenizar una cadena a partid de un delimitador
*/
void split(const string& str, vector<string> &cont, char delim);

#endif // UTILS_H_INCLUDED
