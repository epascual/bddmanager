#include <iostream>
#include <stdlib.h>
#include "BDD.h"
#include "Globals.h"
using namespace std;

bool interp0[] = {0,0,0};
bool interp1[] = {0,0,1};
bool interp2[] = {0,1,0};
bool interp3[] = {0,1,1};
bool interp4[] = {1,0,0};
bool interp5[] = {1,0,1};
bool interp6[] = {1,1,0};
bool interp7[] = {1,1,1};

void example1(){
  cout << "EXAMPLE1 (p and q):\n";
  vector<bool> tt(4,0);

  tt[3] = 1;
  BDD example(2, "pq", tt);
  example.print();
  example.reduce();
  example.print();

  cout << "p | q | p and q\n";
  cout << "0 | 0 |  " << example.evaluate_interpretation(interp0) << "\n";
  cout << "0 | 1 |  " << example.evaluate_interpretation(interp2) << "\n";
  cout << "1 | 0 |  " << example.evaluate_interpretation(interp4) << "\n";
  cout << "1 | 1 |  " << example.evaluate_interpretation(interp6) << "\n";
}

void example2(){
  cout << "EXAMPLE2 (!p):\n";
  vector<bool> tt2(8,0);
  for (int i=0; i < 8; i+=2)tt2[i] = true;
  BDD example2(3, "rqp", tt2);
  example2.print();
  example2.reduce();
  example2.print();

  cout << "r | q | p | !p\n";
  cout << "0 | 0 | 0 | " << example2.evaluate_interpretation(interp0) << "\n";
  cout << "0 | 0 | 1 | " << example2.evaluate_interpretation(interp1) << "\n";
  cout << "0 | 1 | 0 | " << example2.evaluate_interpretation(interp2) << "\n";
  cout << "0 | 1 | 1 | " << example2.evaluate_interpretation(interp3) << "\n";
  cout << "1 | 0 | 0 | " << example2.evaluate_interpretation(interp4) << "\n";
  cout << "1 | 0 | 1 | " << example2.evaluate_interpretation(interp5) << "\n";
  cout << "1 | 1 | 0 | " << example2.evaluate_interpretation(interp6) << "\n";
  cout << "1 | 1 | 1 | " << example2.evaluate_interpretation(interp7) << "\n";
}

void example3(){
  cout << "EXAMPLE3 p or (q and r)=(truthTable = F,F,F,T,T,T,T,T):\n";
  vector<bool> tt3(8,1);
  tt3[0] = tt3[1] = tt3[2] = 0;
  BDD example3(3, "pqr", tt3);
  example3.print();
  example3.reduce();
  example3.print();
}

void example4(){
  cout << "EXAMPLE4 (p xor q xor r):\n";
  vector<bool> tt4(8,0);
  tt4[1] = tt4[2] = tt4[4] = tt4[7] = 1;

  BDD example4(3, "pqr", tt4);
  example4.print();
  example4.reduce();
  example4.print();

  cout << "p | q | r | p xor q xor r\n";
  cout << "0 | 0 | 0 | " << example4.evaluate_interpretation(interp0) << "\n";
  cout << "0 | 0 | 1 | " << example4.evaluate_interpretation(interp1) << "\n";
  cout << "0 | 1 | 0 | " << example4.evaluate_interpretation(interp2) << "\n";
  cout << "0 | 1 | 1 | " << example4.evaluate_interpretation(interp3) << "\n";
  cout << "1 | 0 | 0 | " << example4.evaluate_interpretation(interp4) << "\n";
  cout << "1 | 0 | 1 | " << example4.evaluate_interpretation(interp5) << "\n";
  cout << "1 | 1 | 0 | " << example4.evaluate_interpretation(interp6) << "\n";
  cout << "1 | 1 | 1 | " << example4.evaluate_interpretation(interp7) << "\n";
}

void example5(){
  cout << "EXAMPLE 5: Testing apply on (p xor q) xor (p xor r):\n";
  vector<bool> tt0 = {0,0,1,1,1,1,0,0};
  vector<bool> tt1 = {0,1,0,1,1,0,1,0};
  BDD p_xor_q(3, "pqr", tt0);
  BDD p_xor_r(3, "pqr", tt1);

  BDD applied = p_xor_q.apply(p_xor_r, op_xor);
  applied.print();
  applied.reduce();
  applied.print();
}

/**
  la diferencia del ejemplo 5 con este, es que en el 5 se utilizan
  dos BDDs con los 3 atomos pqr, y ac� uno que solo utiliza pq, y
  otro que utiliza pr
*/
void example6(){
  cout << "EXAMPLE 6: Testing apply on (p xor q) xor (p xor r):\n";
  vector<bool> tt0 = {0,1,1,0};
  vector<bool> tt1 = {0,1,1,0};
  BDD p_xor_q(2, "pq", tt0);
  BDD p_xor_r(2, "pr", tt1);

  BDD applied = p_xor_q.apply(p_xor_r, op_xor);
  applied.print();
  //BDD cop = applied; //Probando constructor de copia
  applied.reduce();
  applied.print();

  //cop.print();
  //cop.reduce();
  //cop.print();
}

void example7(){
  cout << "EXAMPLE7 using ApplyNegation: !(p xor q xor r) :\n";
  vector<bool> tt4(8,0);
  tt4[1] = tt4[2] = tt4[4] = tt4[7] = 1;
  BDD example(3, "pqr", tt4);
  BDD example2 = example.apply_negation();
  example2.print();
  example2.reduce();
  example2.print();

  cout << "p | q | r | !(p xor q xor r)\n";
  cout << "0 | 0 | 0 | " << example2.evaluate_interpretation(interp0) << "\n";
  cout << "0 | 0 | 1 | " << example2.evaluate_interpretation(interp1) << "\n";
  cout << "0 | 1 | 0 | " << example2.evaluate_interpretation(interp2) << "\n";
  cout << "0 | 1 | 1 | " << example2.evaluate_interpretation(interp3) << "\n";
  cout << "1 | 0 | 0 | " << example2.evaluate_interpretation(interp4) << "\n";
  cout << "1 | 0 | 1 | " << example2.evaluate_interpretation(interp5) << "\n";
  cout << "1 | 1 | 0 | " << example2.evaluate_interpretation(interp6) << "\n";
  cout << "1 | 1 | 1 | " << example2.evaluate_interpretation(interp7) << "\n";
}

void example8(){
  cout << "EXAMPLE8 Restrict: p or (q and r) :\n";
  vector<bool> tt4 = {0,0,0,1,1,1,1,1};
  BDD example(3, "pqr", tt4);

  example.reduce();
  example.print();

  BDD t = example.restrict('r', true);
  t.print();
  BDD f = example.restrict('r', false);
  f.print();
  f.reduce();
  f.print();
}

void example9(){
  cout << "EXAMPLE9 :\n";
  vector<bool> tt1 = {1,1,0,0,0,0,0,0};
  vector<bool> tt2 = {0,0,1,1,1,0,0,0};
  vector<bool> tt3 = {0,0,0,0,0,1,1,1};
  BDD bdd1(3, "pqr", tt1);
  BDD bdd2(3, "pqr", tt2);
  BDD bdd3(3, "pqr", tt3);

  BDD x = bdd1.apply(bdd2, op_or);
  BDD y = x.apply(bdd3, op_or);
  x.reduce();
  x.print();
  y.reduce();
  y.print();
}

/*
En este ejemplo se construyen todos los posibles BDDS para los literales
pqrs. En total son 65536 BBDs diferentes.
Recomendaci�n: No imprimir en pantalla se demora bastante
*/
void longExample(){
  vector<bool> flag(16,0);
  for (int i=0; i < (1<<16); i++){
    for (int j=0; j < 16; j++){
      flag[j] = (i & (1 << j));
    }
    BDD a(4, "pqrs", flag);
    for (int j=0; j < 16; j++){
      cout << flag[j] << " ";
    }
    cout << "\n";
    a.reduce();
    a.print();
    //scanf("%*c");
  }
}


//CLI
void userApplication();

int main(){
  cout << " *** Ejecutando ejempos!\n";

  example1();
  example2();
  example3();
  example4();
  example5();
  example6();
  example7();
  example8();
  example9();

  cout << " *** Fin de ejemplos\n\n";
////  longExample();
  userApplication();

  return 0;
}

