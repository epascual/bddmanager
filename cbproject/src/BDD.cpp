#include "BDD.h"
#include "Constants.h"
#include <cstring>
#include <assert.h>
#include <cmath>

BDD::BDD(int N, string atoms, vector<bool> &truthTable)
{
  root = nullptr;

  {///Preconditions checker
    ///Inner block for variables scope
    int log2=0, tmp = truthTable.size();
    while (tmp>1){
        log2++; tmp/=2;
    }
    assert(N <= (int)atoms.size());
    assert(N <= log2);
  }

  this->atoms = atoms;
  Node *True = new Node(TRUE_LITERAL, nullptr);
  Node *False = new Node(FALSE_LITERAL, nullptr);

  numLeaf = 0;
  root = make_tree(0, N, atoms.c_str(), truthTable, nullptr, True, False);

  ///Por si alguna raz�n queda hu�rfana
  if (True->getParents().size() == 0){
    delete True;
  }
  if (False->getParents().size() == 0){
    delete False;
  }
}

Node * BDD::deep_copy(Node *nod, Node* parent, map<Node *, Node *> &oldToNewNode){
  if (nod == nullptr)
    return nullptr;

  Node *otherNod;
  if (oldToNewNode.find(nod) != oldToNewNode.end()){
    otherNod = oldToNewNode[nod];
    otherNod->addParent(parent);
  } else {
    otherNod = new Node(nod->getAtom(), parent);
    oldToNewNode[nod] = otherNod;
    otherNod->setLeftSon(deep_copy(nod->getLeftSon(), otherNod, oldToNewNode));
    otherNod->setRightSon(deep_copy(nod->getRightSon(), otherNod, oldToNewNode));
  }

  return otherNod;
}

BDD::BDD(const BDD &otro){
  atoms = otro.atoms;

  map<Node *, Node *> oldToNewNode;

  root = deep_copy(otro.root, nullptr, oldToNewNode);
}

BDD::~BDD()
{
  free_memory(root);
}

Node * BDD::make_tree(int depth, int N, string atoms, const vector<bool> &truthTable,
                      Node *parent, Node *True, Node * False){
  Node * nod;

  if (depth == N){
    nod = truthTable[numLeaf] ? True : False;
    nod->addParent(parent);
    numLeaf++;
  } else {
    nod = new Node(atoms[depth], parent);
    nod->setLeftSon(make_tree(depth+1, N, atoms, truthTable, nod, True, False));
    nod->setRightSon(make_tree(depth+1, N, atoms, truthTable, nod, True, False));
  }

  return nod;
}

string BDD::getAtoms(){
  return atoms;
}

bool BDD::evaluate_interpretation(bool interpretation[]){
  return evaluate_interpretation_aux(root, interpretation);
}

bool BDD::evaluate_interpretation(const vector<bool> &interpretation){
  assert(interpretation.size() >= atoms.size());
  return evaluate_interpretation_aux(root, interpretation);
}

bool BDD::evaluate_interpretation_aux(Node *nod, bool interpretation[]){
  if (nod->getAtom() == TRUE_LITERAL)
    return true;
  if (nod->getAtom() == FALSE_LITERAL)
    return false;

  int literalPos = strchr(atoms.c_str(), nod->getAtom()) - atoms.c_str();
  if (interpretation[literalPos]){
    return evaluate_interpretation_aux(nod->getRightSon(), interpretation);
  } else {
    return evaluate_interpretation_aux(nod->getLeftSon(), interpretation);
  }
}

bool BDD::evaluate_interpretation_aux(Node *nod, const vector<bool> &interpretation){
  if (nod->getAtom() == TRUE_LITERAL)
    return true;
  if (nod->getAtom() == FALSE_LITERAL)
    return false;

  int literalPos = strchr(atoms.c_str(), nod->getAtom()) - atoms.c_str();
  if (interpretation[literalPos]){
    return evaluate_interpretation_aux(nod->getRightSon(), interpretation);
  } else {
    return evaluate_interpretation_aux(nod->getLeftSon(), interpretation);
  }
}

void BDD::free_memory(Node * nod){
  if (nod != nullptr){
    free_memory(nod->getLeftSon());
    free_memory(nod->getRightSon());
    nod->deleteFromParents();
    delete(nod);
  }
}
void BDD::free_orphans(Node *nod){
  vector<Node *> empty;
  if (nod->getLeftSon() != nullptr){
    nod->getLeftSon()->changeParent(nod, empty);
    if (nod->getLeftSon()->getParents().size() == 0){
      free_orphans(nod->getLeftSon());
    }
  }
  if (nod->getRightSon() != nullptr){
    nod->getRightSon()->changeParent(nod, empty);
    if (nod->getRightSon()->getParents().size() == 0){
      free_orphans(nod->getRightSon());
    }
  }
  delete nod;
}

void BDD::reduce(){
  while (step1(root) || (step2(root))){
    ///be happy
  }
}

bool BDD::step1(Node * nod){
  if (nod == nullptr){
    return false;
  }

  if (step1(nod->getLeftSon()))
    return true;
  if (step1(nod->getRightSon()))
    return true;

  if (nod->getLeftSon() == nod->getRightSon() && (nod->getLeftSon() != nullptr)){
    nod->getLeftSon()->changeParent(nod, nod->getParents());
    nod->replaceInParents(nod->getLeftSon());

    ///Caso especial, el nodo eliminado es la raiz debe actualizarse ptro root
    if (nod == root)
      root = nod->getLeftSon();

    delete nod;
    return true;
  }
  return false;
}


bool BDD::step2(Node * nod){
  if (nod->getAtom() == TRUE_LITERAL){
    return false;
  }
  if (nod->getAtom() == FALSE_LITERAL){
    return false;
  }

  if (step2(nod->getLeftSon())){
    return true;
  }
  if (step2(nod->getRightSon())){
    return true;
  }

  Node *otro = step2_aux(root, nod);

  if (otro != nullptr){
    nod->changeParent(nullptr, otro->getParents());
    otro->replaceInParents(nod);
    free_orphans(otro);
    return true;
  }

  return false;
}

Node * BDD::step2_aux(Node *nod, const Node *ref){
  if (nod->getAtom() == TRUE_LITERAL)
    return nullptr;
  if (nod->getAtom() == FALSE_LITERAL)
    return nullptr;

  if (nod != ref && (*nod) == (*ref)){
    return nod;
  }
  Node *l = step2_aux(nod->getLeftSon(), ref);
  if (l != nullptr)
    return l;

  Node *r = step2_aux(nod->getRightSon(), ref);
  if (r != nullptr)
    return r;

  return nullptr;
}

int BDD::size(){
  set<Node*> allNodes;

  size_aux(root, allNodes);

  return allNodes.size();
}

void BDD::size_aux(Node *nod, set<Node *> &allNodes){
  if (nod != nullptr){
    allNodes.insert(nod);
    size_aux(nod->getLeftSon(), allNodes);
    size_aux(nod->getRightSon(), allNodes);
  }
}

bool BDD::is_before_in_ordering(char lit1, char lit2, const string &atoms){
  if (IS_LITERAL(lit1) || IS_LITERAL(lit2))
    return false;
  int diff = strchr(atoms.c_str(), lit1) - strchr(atoms.c_str(), lit2);
  return diff < 0;
}

Node* BDD::apply_aux(Node *bdd1, Node *bdd2, const string &atoms, vector<bool> op, Node *parent, Node *True, Node *False){
  char lit1 = bdd1->getAtom(), lit2 = bdd2->getAtom();
  Node * nod = nullptr;

  if (IS_LITERAL(lit1) && IS_LITERAL(lit2)){
    int pos;
    if (lit1 == FALSE_LITERAL && lit2 == FALSE_LITERAL) pos=0;
    else if (lit1 == FALSE_LITERAL && lit2 == TRUE_LITERAL) pos=1;
    else if (lit1 == TRUE_LITERAL && lit2 == FALSE_LITERAL) pos=2;
    else pos=3;

    nod = op[pos] ? True : False;
    nod->addParent(parent);
  } else if (lit1 == lit2) {
    nod = new Node(lit1, parent);
    nod->setLeftSon(apply_aux(bdd1->getLeftSon(),bdd2->getLeftSon(), atoms, op, nod, True, False));
    nod->setRightSon(apply_aux (bdd1->getRightSon(),bdd2->getRightSon(), atoms, op, nod, True, False));
  } else if (IS_LITERAL(lit2) || is_before_in_ordering(lit1, lit2, atoms)){
    nod = new Node(lit1, parent);
    nod->setLeftSon(apply_aux(bdd1->getLeftSon(), bdd2, atoms, op, nod, True, False));
    nod->setRightSon(apply_aux(bdd1->getRightSon(), bdd2, atoms, op, nod, True, False));
  } else {
    nod = new Node(lit2, parent);
    nod->setLeftSon(apply_aux(bdd1, bdd2->getLeftSon(), atoms, op, nod, True, False));
    nod->setRightSon(apply_aux(bdd1, bdd2->getRightSon(), atoms, op, nod, True, False));
  }

  return nod;
}

BDD BDD::apply(const BDD &bdd2, vector<bool> op){
  ///TODO: Primero se deber�a chequear consistencia de ordenamientos
  assert(op.size() >= 4);

  Node *True = new Node(TRUE_LITERAL, nullptr);
  Node *False = new Node(FALSE_LITERAL, nullptr);

  string compatible_order = make_compatible_order(atoms, bdd2.atoms);

  BDD ret;
  ret.atoms = compatible_order;
  ret.root = apply_aux(root, bdd2.root, compatible_order, op, nullptr, True, False);

  ///Por si alguna raz�n queda hu�rfana
  if (True->getParents().size() == 0){
    delete True;
  }
  if (False->getParents().size() == 0){
    delete False;
  }

  return ret;
}

void BDD::find_atoms_nodes(Node * nod, Node * &trueNode, Node * &falseNode){
  if (nod->getAtom() == TRUE_LITERAL){
    trueNode = nod;
  } else if (nod->getAtom() == FALSE_LITERAL){
    falseNode = nod;
  } else {
    find_atoms_nodes(nod->getLeftSon(), trueNode, falseNode);
    find_atoms_nodes(nod->getRightSon(), trueNode, falseNode);
  }
}

BDD BDD::apply_negation(){
  BDD otro = *this;
  Node *trueNode = nullptr;
  Node *falseNode = nullptr;
  find_atoms_nodes(otro.root, trueNode, falseNode);
  if (trueNode != nullptr)
    trueNode->setAtom(FALSE_LITERAL);
  if (falseNode != nullptr)
    falseNode->setAtom(TRUE_LITERAL);
  return otro;
}

void BDD::restrict_aux(Node *nod, char atom, bool w){
  if (IS_LITERAL(nod->getAtom())){
    return;
  }
  if (nod->getAtom() == atom){
    if (!w){
      nod->getLeftSon()->changeParent(nod, nod->getParents());
      nod->replaceInParents(nod->getLeftSon());
    } else {
      nod->getRightSon()->changeParent(nod, nod->getParents());
      nod->replaceInParents(nod->getRightSon());
    }
    if (nod == root){
      if (!w){
        root = nod->getLeftSon();
      } else {
        root = nod->getRightSon();
      }
      delete nod;
    }
    return;
  }
  restrict_aux(nod->getLeftSon(), atom, w);
  restrict_aux(nod->getRightSon(), atom, w);
}

BDD BDD::restrict(char atom, bool w){
  char *ptr = strchr(atoms.c_str(), atom);
  ///Garantizando que se reduzca un atomo del BDD
  assert(ptr != NULL);

  BDD otro = *this;
  ///Eliminando el atomo restringido de la lista de atomos
  otro.atoms.replace(ptr-atoms.c_str(), 1, "");

  otro.restrict_aux(otro.root, atom, w);

  return otro;
}

/*Impresi�n del BDD*/

void BDD::print(){
  printf(" %d ", size());
  traverse(root, 0);
  printf("\n");
}

void BDD::traverse(Node *nod, bool flag){
  if (flag){
    printf("( %p ", nod);
  } else {
    printf("( %c ", nod->getAtom());
  }
  if (nod->getLeftSon() != nullptr){
    traverse(nod->getLeftSon(), flag);
  }
  if (nod->getRightSon() != nullptr){
    traverse(nod->getRightSon(), flag);
  }
  printf(")");
}
