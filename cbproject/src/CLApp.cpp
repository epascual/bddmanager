#include<iostream>
#include<map>
#include"BDD.h"
#include"Utils.h"
#include"Globals.h"
#include <stdlib.h>

using namespace std;

map<string, BDD*> lista_bdds;

void printMenu(){
  cout << "\nSeleccione la operacion:\n";
  cout << "\t1) Imprimir BDDs declarados.\n";
  cout << "\t2) Declarar nuevo BDD.\n";
  cout << "\t3) Aplicar operador entre dos BDDs declarados.\n";
  cout << "\t0) Salir del programa\n";
  cout << "?- ";
}

void readNewBDD(){
  string name;
  string atoms;
  string trueValues;
  vector<bool> truthTable;
  cout << "Introduzca el nombre del nuevo BDD: ";

  getline(cin, name);

  if (lista_bdds.find(name) != lista_bdds.end()) {
      cout << "Error! Ya existe un BDD con el nombre " << name << "\n";
      return;
  }
  cout << "Introduzca los atomos del BDD " << name << ": ";

  getline(cin, atoms);
  for (int i=0; atoms[i]; i++) {
    if (atoms[i] == TRUE_LITERAL || atoms[i] == FALSE_LITERAL){
      cout << "Literal invalido en posici�n " << i+1 << ": " << atoms[i] << "\n";
      return;
    }
  }

  int N = atoms.size();
  cout << "Introduzca los " << (1 << N) << " valores de verdad de la formula: (0s y 1s separados por espacio)\n";
  getline(cin, trueValues);

  vector<string> truthTableStr;
  split(trueValues, truthTableStr, ' ');

  if ((int)truthTableStr.size() != (1<<N)){
    cout << "No introdujo exactamente " << (1<<N) << " valores.\n";
    return;
  }

  for (int i=0; i < (int)truthTableStr.size(); i++){
    if (truthTableStr[i] == "1"){
      truthTable.push_back(true);
    } else if (truthTableStr[i] == "0"){
      truthTable.push_back(false);
    } else {
      cout << "El valor " << i+1 << " de verdad no es ni 0 ni 1.\n";
      return ;
    }
  }

  BDD * bdd = new BDD(N, atoms, truthTable);
  bdd->reduce();
  lista_bdds[name] = bdd;
}

void applyOperator(){
  string line;
  string nameA, nameB;

  cout << "Elegiste aplicar un operador entre dos BDDs: (A)op(B)\n";

  cout << "Introduzca el nombre del BDD A: ";
  getline(cin, nameA);
  if (lista_bdds.find(nameA) == lista_bdds.end()) {
      cout << "Error! No existe un BDD con el nombre " << nameA << "\n";
      return;
  }

  cout << "Introduzca el nombre del BDD B: ";
  getline(cin, nameB);
  if (lista_bdds.find(nameB) == lista_bdds.end()) {
      cout << "Error! No existe un BDD con el nombre " << nameB << "\n";
      return;
  }

  BDD *A = lista_bdds[nameA];
  BDD *B = lista_bdds[nameB];

  cout << "Seleccione el operador:\n";
  cout << "\t1) AND\n";
  cout << "\t2) OR\n";
  cout << "\t3) IMPLICACION\n";
  cout << "\t4) EQUIVALENCE\n";
  cout << "\t5) NOR\n";
  cout << "\t6) NAND\n";
  cout << "\t7) XOR\n";
  cout << "?- ";

  getline(cin, line);
  int opcion = atoi(line.c_str());

  BDD *c;

  switch (opcion) {
    case 1:
      c = new BDD(A->apply(*B,op_and));
      break;
    case 2:
      c = new BDD(A->apply(*B,op_or));
      break;
    case 3:
      c = new BDD(A->apply(*B,op_implication));
      break;
    case 4:
      c = new BDD(A->apply(*B,op_equivalence));
      break;
    case 5:
      c = new BDD(A->apply(*B,op_nor));
      break;
    case 6:
      c = new BDD(A->apply(*B,op_nand));
      break;
    case 7:
      c = new BDD(A->apply(*B,op_xor));
      break;
    default:
      cout << "Opcion no valida\n";
      return;
      break;
  }

  ///reduciendo resultado
  c->reduce();
  cout << "El resultado es:\n";
  c->print();

  cout << "Inserte un nombre para guardar el resultado: ";
  getline(cin, line);

  if (lista_bdds.find(line) != lista_bdds.end()) {
      cout << "Error! Ya existe un BDD con el nombre " << line << "\n";
      delete c;
      return;
  }
  lista_bdds[line] = c;
}

void printBDDs(){
  cout << "Imprimiendo los " << lista_bdds.size() << " BDDs\n";
  map<string, BDD*>::iterator it;

  int cont;
  for (it = lista_bdds.begin(), cont=1; it != lista_bdds.end(); it++, cont++){
    cout << "\tBDD #" << cont << " \"" << it->first << "\" con atomos \"" + it->second->getAtoms() + "\"\n";
    it->second->print();
  }

  cout << "Fin de impresion.\n";
}

void userApplication(){
  cout << "BDD Manager by Eduardo Pascual v1.0\n";

  bool running = true;
  string line;
  while (running) {
    printMenu();
    getline(cin, line);

    int opcion = atoi(line.c_str());

    switch (opcion){
    case 0:
      running = false;
      break;
    case 1:
      printBDDs();
      break;
    case 2:
      readNewBDD();
      break;
    case 3:
      applyOperator();
      break;
    default:
      break;
    }
  }

  cout << "Cerrando BDD Manager by Eduardo Pascual v1.0\n";
}

