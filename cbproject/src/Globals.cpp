#include "Globals.h"

vector<bool> op_or = {0,1,1,1};
vector<bool> op_and = {0,0,0,1};
vector<bool> op_implication = {1,1,0,1};
vector<bool> op_equivalence = {1,0,0,1};
vector<bool> op_nor = {1,0,0,0};
vector<bool> op_nand = {1,1,1,0};
vector<bool> op_xor = {0,1,1,0};
