#include "Node.h"
#include "Constants.h"

Node::Node(char atom, Node *parent)
{
  this->atom = atom;
  if (parent != nullptr)
    parents.push_back(parent);
  leftSon = rightSon = nullptr;
}

Node::~Node()
{
  //dtor
}

bool Node::operator == (const Node & o) const {
  if (atom != o.atom)
    return false;

  ///o ambos hijoz izquierdos son null o ninguno
  if ((leftSon != nullptr && o.leftSon == nullptr) || (leftSon == nullptr && o.leftSon != nullptr))
    return false;
  ///si tienen hijoz izquierdos deben ser iguales
  if (leftSon != nullptr && !((*leftSon)==(*o.leftSon)))
    return false;

  ///Similar a lo anterior pero con hijos derechos
  if ((rightSon != nullptr && o.rightSon == nullptr) || (rightSon == nullptr && o.rightSon != nullptr))
    return false;
  if (rightSon != nullptr && !((*rightSon)==(*o.rightSon)))
    return false;

  return true;
}

void Node::setLeftSon(Node *son){
  leftSon = son;
}
void Node::setRightSon(Node *son){
  rightSon = son;
}

Node * Node::getLeftSon() const{
  return leftSon;
}
Node * Node::getRightSon() const{
  return rightSon;
}

vector<Node *> Node::getParents() const{
  return parents;
}
void Node::addParent(Node *newParent){
  if (newParent != nullptr)
    parents.push_back(newParent);
}

char Node::getAtom() const{
  return atom;
}

void Node::setAtom(char newAtom) {
  atom = newAtom;
}

void Node::deleteFromParents(){
  for (Node * &parent: parents){
    if (parent == nullptr)
      continue;
    if (parent->leftSon == this){
      parent->leftSon = nullptr;
    }
    if (parent->rightSon == this){
      parent->rightSon = nullptr;
    }
  }
}

void Node::changeParent(Node *oldParent, const vector<Node*> &newParents){
  vector<Node *> parents2;
  for (Node * parent: parents){
    if (parent != nullptr && parent != oldParent){
      parents2.push_back(parent);
    }
  }
  parents2.insert(parents2.end(), newParents.begin(), newParents.end());
  parents = parents2;
}

void Node::replaceInParents(Node *newSon){
  for (Node * &parent: parents){
    if (parent->getLeftSon() == this)
      parent->setLeftSon(newSon);
    if (parent->getRightSon() == this)
      parent->setRightSon(newSon);
  }
  parents = vector<Node *>();
}
