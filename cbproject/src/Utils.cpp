#include "Utils.h"

bool appears_in(const char *ptr, char x){
  return strchr(ptr, x) != nullptr;
}

string make_compatible_order(const string &order1, const string &order2){
  string res = "";
  int p1 = 0, p2 = 0;

  while (p1 < (int)order1.size() && p2 < (int)order2.size()){
    if (order1[p1] == order2[p2]){
      res += order1[p1];
      p1++;
      p2++;
    } else if (appears_in(order2.c_str() + p2, order1[p1]) && !appears_in(order1.c_str() + p1, order2[p2])){
      res += order2[p2];
      p2++;
    } else if (!appears_in(order2.c_str() + p2, order1[p1]) && appears_in(order1.c_str() + p1, order2[p2])){
      res += order1[p1];
      p1++;
    } else if (!appears_in(order2.c_str() + p2, order1[p1]) && !appears_in(order1.c_str() + p1, order2[p2])){
      ///Selecciono cualquiera
      res += order1[p1];
      p1++;
    } else {
      assert(false && "No compatible order");
    }
  }
  while (p1 < (int)order1.size()){
    res += order1[p1];
    p1++;
  }
  while (p2 < (int)order2.size()){
    res += order2[p2];
    p2++;
  }
  return res;
}


///Funcion practicamente copiada y pegada desde:
///http://www.martinbroadhurst.com/how-to-split-a-string-in-c.html
void split(const string& str, vector<string> &cont, char delim)
{
    size_t current, previous = 0;
    current = str.find(delim);
    while (current != string::npos) {
        cont.push_back(str.substr(previous, current - previous));
        previous = current + 1;
        current = str.find(delim, previous);
    }
    cont.push_back(str.substr(previous, current - previous));
}
